import timeit

def counted(f):
    def wrapped(*args, **kwargs):
        wrapped.calls += 1
        return f(*args, **kwargs)
    wrapped.calls = 0
    return wrapped

def timing(f):
    def wrapped(*args, **kwargs):
        wrapped.calls += 1
        wrapped.start = timeit.default_timer()
        res = f(*args, **kwargs)
        wrapped.end = timeit.default_timer()
        wrapped.timelapse = float(wrapped.end - wrapped.start)
        wrapped.totaltime += wrapped.timelapse
        wrapped.avgtime = wrapped.totaltime / float(wrapped.calls)
        return res
    wrapped.calls = 0
    wrapped.totaltime = 0
    wrapped.avgtime = 0
    return wrapped
