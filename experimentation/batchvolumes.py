
import os, sys
cwd = os.getcwd()
sys.path.append(cwd)

import polytopesgenerator as polygen
import polytope as poly
import qosranker
import argparse
from iotools import IOTools
import subprocess

parser = argparse.ArgumentParser(description="Run a batch of volume estimation over regions and save all fine-grained results.")
parser.add_argument('dim_start', help='dimension interval (start)', type=int)
parser.add_argument('dim_stop', help='dimension interval (stop)', type=int)
parser.add_argument('dim_step', help='dimension interval (step)', type=int)
parser.add_argument('n_start', help='number of polytopes of region (start)', type=int)
parser.add_argument('n_stop', help='number of polytopes of region (stop)', type=int)
parser.add_argument('n_step', help='number of polytopes of region (step)', type=int)
parser.add_argument('input_dir', help='path to input dir, regions will be read from input_dir/$dim$d/$n$n/casenumber')
parser.add_argument('output_dir', help='path to output dir, region will be saved as outputdir/$dim$d/$n$n/casenumber')
parser.add_argument('-ncase_start', help='Case numbers for each size', default = 1, type=int)
parser.add_argument('-ncase_stop', help='Case numbers for each size', default = 1, type=int)
parser.add_argument('-ncase_step', help='Case numbers for each size', default = 1, type=int)
parser.add_argument('-timeout', help='Timeout for the volume estimation of each region', default = 3600, type=int)
parser.add_argument('-tolerance', help='Tolerance for error (0 - 1)', default = 0, type=float)
args = parser.parse_args()

timeouted = False

for d in range(args.dim_start, args.dim_stop+1, args.dim_step):
    timeouted = False
    for n in range(args.n_start, args.n_stop+1, args.n_step):
        if timeouted:
            break
        output_dir = args.output_dir + str(d)+"d/" + str(n) + "n/"
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        for case in range(args.ncase_start, args.ncase_stop+1, args.ncase_step):
            input_dir = args.input_dir + str(d)+"d/" + str(n) + "n/"

            IOTools.output("Dim: {}, N: {}, Case: {}, Tolerance: {}".format(d,n,case, args.tolerance))

            command = ['python3', 'experimentation/regionvolume.py', input_dir + str(case), output_dir + str(case), '-tolerance', str(args.tolerance)]

            try:
                subprocess.run(command, timeout=args.timeout)
            except subprocess.TimeoutExpired:
                with open(output_dir + str(case), 'w') as f:
                    f.write("-1\n")
                    IOTools.output("Timeout {}.".format(str(args.timeout)))
                timeouted = True
                break
            else:
                IOTools.output("Fin.")
