import os
import sys
import argparse
cwd = os.getcwd()
sys.path.append(cwd)

def read_results_batch(d_start, d_stop, d_step, n_start, n_stop, n_step, dir, case_start, case_stop, lnum, sep):

    for case in range(case_start, case_stop + 1):
        for n in range(n_start, n_stop + 1, n_step):
            for d in range(d_start, d_stop + 1, d_step):

                f_res = dir + str(d) + 'd/' + str(n) + 'n/' + str(case)

                try:
                    f = open(f_res, 'r')
                except IOError:
                    data = '-'
                else:
                    content = f.readlines()
                    if content[0] == '-1':
                        data = '-1'
                    else:
                        data = '{:.2f}'.format(float(content[lnum]))
                if d != d_start:
                    sys.stdout.write(' ' + sep + ' ')
                sys.stdout.write(data)

            sys.stdout.write("\n")

parser = argparse.ArgumentParser(description="Reads and presents results of volume estimation.")
parser.add_argument('d_start', help='d start', type=int)
parser.add_argument('d_stop', help='d stop', type=int)
parser.add_argument('d_step', help='d step', type=int)
parser.add_argument('n_start', help='n start', type=int)
parser.add_argument('n_stop', help='n stop', type=int)
parser.add_argument('n_step', help='n step', type=int)
parser.add_argument('-dir', help='Directory of results ("experimentation/results-vol/")', default="experimentation/results-vol/")
parser.add_argument('-case_start', help='Case number (default=1)', default=1, type=int)
parser.add_argument('-case_stop', help='Case number (default=1)', default=1, type=int)
parser.add_argument('-lnum', help='Line number to read (-1) (-1 is last)', default=-1, type=int)
parser.add_argument('-sep', help='Separator to print ("")', default='')
args = parser.parse_args()

read_results_batch(args.d_start, args.d_stop, args.d_step, args.n_start, args.n_stop, args.n_step, args.dir, args.case_start, args.case_stop, args.lnum, args.sep)
