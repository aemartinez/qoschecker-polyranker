
import os, sys
cwd = os.getcwd()
sys.path.append(cwd)

import polytopesgenerator as polygen
import polytope as poly
import qosranker
import argparse
from iotools import IOTools

parser = argparse.ArgumentParser(description="Ranks a qos compliance and stores the result.")
parser.add_argument('prov_file', help='prov filename')
parser.add_argument('req_file', help='req filename')
parser.add_argument('output_file', help='output filename')
parser.add_argument('-tolerance', help='error tolerance (0.0 - 1.0)', default=0, type = float)
parser.add_argument('-previous_file', help='file with precalculated previous iterations', default='')
args = parser.parse_args()

log_coverage_volume = []
log_coverage_errors = []
log_prov_volume = []
log_prov_errors = []
if args.previous_file != '':
    with open(args.previous_file) as file:
        lines = file.readlines()
        log_coverage_volume = eval(lines[1])
        log_coverage_errors = eval(lines[3])
        log_prov_volume = eval(lines[5])
        log_prov_errors = eval(lines[7])

ranker = qosranker.QoSRanker()
ranking = ranker.compute_ranking(args.prov_file, args.req_file, args.tolerance, log_coverage_volume, log_coverage_errors, log_prov_volume, log_prov_errors)

timelapse = sum([e[2] for e in log_coverage_volume]) + sum([e[2] for e in log_prov_volume])

with open(args.output_file, 'w') as f:
    f.write("log_coverage_volume\n")
    f.write("{}\n".format(ranking['log_coverage_volume']))
    f.write("log_coverage_errors\n")
    f.write("{}\n".format(ranking['log_coverage_errors']))
    f.write("log_prov_volume\n")
    f.write("{}\n".format(ranking['log_prov_volume']))
    f.write("log_prov_errors\n")
    f.write("{}\n".format(ranking['log_prov_errors']))
    f.write("coverage_volume, {:.3f}\n".format(ranking['coverage_volume']))
    f.write("coverage_error_bound, {:.3f}\n".format(ranking['coverage_error_bound']))
    f.write("prov_volume, {:.3f}\n".format(ranking['prov_volume']))
    f.write("prov_error_bound, {:.3f}\n".format(ranking['prov_error_bound']))
    f.write("coverage_steps,{}\n".format(ranking['log_coverage_volume'][-1][0]))
    f.write("prov_steps,{}\n".format(ranking['log_prov_volume'][-1][0]))
    f.write("estimator_calls, {}\n".format(ranking['estimator_calls']))
    f.write("rank, {:.6f}\n".format(ranking['ranking']))
    f.write("timelapse, {:.2f}\n".format(timelapse))
