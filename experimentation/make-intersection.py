import os, sys
cwd = os.getcwd()
sys.path.append(cwd)

import polytopesgenerator as polygen
import polytope as poly
import qosranker
import argparse
from iotools import IOTools

parser = argparse.ArgumentParser(description="Save intersection of two regions in output file.")
parser.add_argument('region1_file', help='region filename')
parser.add_argument('region2_file', help='region filename')
parser.add_argument('output_file', help='output filename')
args = parser.parse_args()

r1 = poly.Region.from_file(args.region1_file)
r2 = poly.Region.from_file(args.region2_file)
inn = r1.intersect_with(r2)
inn.to_file(args.output_file)
