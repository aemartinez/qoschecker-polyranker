
import os, sys
cwd = os.getcwd()
sys.path.append(cwd)

import polytopesgenerator as polygen
import polytope as poly
import qosranker
import argparse
from iotools import IOTools
import subprocess

parser = argparse.ArgumentParser(description="Run a batch of reprocessing qos ranking results with new tolerance.")
parser.add_argument('dim', help='dimension', type=int)
parser.add_argument('n', help='number of polytopes of provision', type=int)
parser.add_argument('m', help='number of polytopes of requirement', type=int)
parser.add_argument('input_dir', help='path to input dir, previous results will be read from input_dir/$dim$d/$n$n/$m$m/casenumber/')
parser.add_argument('output_dir', help='path to output dir, new results will be saved as outputdir/$dim$d/$n$n/$m$m/casenumber/')
parser.add_argument('-ncase_start', help='Case numbers', default = 1, type=int)
parser.add_argument('-ncase_stop', help='Case numbers', default = 1, type=int)
parser.add_argument('-ncase_step', help='Case numbers', default = 1, type=int)
parser.add_argument('-tolerance', help='error tolerance (0.0 - 1.0)', default=0, type = float)
args = parser.parse_args()

for casenumber in range(args.ncase_start, args.ncase_stop+1, args.ncase_step):

    full_results_dir = args.input_dir + str(args.dim)+"d/" + str(args.n)+"n/" + str(args.m)+"m/" + str(casenumber) +"/"
    full_output_dir = args.output_dir + str(args.dim)+"d/" + str(args.n)+"n/" + str(args.m)+"m/" + str(casenumber) +"/"
    if not os.path.exists(full_output_dir):
        os.makedirs(full_output_dir)
    req = 21 #hardcodeado, tengo 21 regiones, tomo 20 como provs y la 21 como req.
    for i in range(1, 21):
        IOTools.output("Dim: {}, N: {}, M: {}, Case: {}, Prov: {}, ".format(args.dim,args.n ,args.m,casenumber, i))
        command = ['python3', 'experimentation/reprocess-inclusion-ratio-result.py', str(args.tolerance), full_results_dir + str(i), full_output_dir + str(i)]
        print(command)
        subprocess.run(command)
        IOTools.output("Fin.")
