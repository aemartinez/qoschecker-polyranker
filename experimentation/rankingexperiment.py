
import os, sys
cwd = os.getcwd()
sys.path.append(cwd)

import polytopesgenerator as polygen
import polytope as poly
import qosranker
import argparse
from iotools import IOTools
import subprocess

parser = argparse.ArgumentParser(description="Run a batch of ranking qos and stores the result.")
parser.add_argument('dim', help='dimension', type=int)
parser.add_argument('n', help='number of polytopes of provision', type=int)
parser.add_argument('m', help='number of polytopes of requirement', type=int)
parser.add_argument('input_dir', help='path to input dir, regions will be read from input_dir/$dim$d/$n$n/casenumber/')
parser.add_argument('output_dir', help='path to output dir, region will be saved as outputdir/$dim$d/$n$n/casenumber/')
parser.add_argument('-ncase_start', help='Case numbers', default = 1, type=int)
parser.add_argument('-ncase_stop', help='Case numbers', default = 1, type=int)
parser.add_argument('-ncase_step', help='Case numbers', default = 1, type=int)
parser.add_argument('-timeout', help='Timeout for each ranking process', default = 3600, type=int)
parser.add_argument('-tolerance', help='error tolerance (0.0 - 1.0)', default=0, type = float)
args = parser.parse_args()

for casenumber in range(args.ncase_start, args.ncase_stop+1, args.ncase_step):

    full_input_dir_prov = args.input_dir + str(args.dim)+"d/" + str(args.n)+"n/" + str(casenumber) +"/"
    full_input_dir_req = args.input_dir + str(args.dim)+"d/" + str(args.m)+"n/" + str(casenumber) +"/"
    full_output_dir = args.output_dir + str(args.dim)+"d/" + str(args.n)+"n/" + str(args.m)+"m/" + str(casenumber) +"/"
    if not os.path.exists(full_output_dir):
        os.makedirs(full_output_dir)
    req = 21 #hardcodeado, tengo 21 regiones, tomo 20 como provs y la 21 como req.
    for i in range(1, 21):
        filename_prov = i
        filename_req = req
        IOTools.output("Dim: {}, N: {}, M: {}, Case: {}, Prov: {}, ".format(args.dim,args.n ,args.m,casenumber, i))

        command = ['python3', 'experimentation/rankerproc.py', full_input_dir_prov + str(filename_prov), full_input_dir_req + str(filename_req), full_output_dir + str(filename_prov), '-tolerance', str(args.tolerance)]
        print(command)
        try:
            subprocess.run(command, timeout=args.timeout)
        except subprocess.TimeoutExpired:
            with open(full_output_dir + str(filename_prov), 'w') as f:
                f.write("-1")
                IOTools.output("Timeout.")
            timeouted = True
            break
        else:
            IOTools.output("Fin.")
