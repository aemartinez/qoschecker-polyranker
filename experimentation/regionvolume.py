
import os, sys
cwd = os.getcwd()
sys.path.append(cwd)

import polytopesgenerator as polygen
import polytope as poly
import qosranker
import argparse
from iotools import IOTools

parser = argparse.ArgumentParser(description="Computes volume estimation over one region and save all fine-grained results.")
parser.add_argument('region_file', help='region filename')
parser.add_argument('output_file', help='output filename')
parser.add_argument('-tolerance', help='error tolerance', default=0, type=float)
args = parser.parse_args()

r = poly.Region.from_file(args.region_file)

# if args.tolerance == 0:
#     ps_vols_times = r.volume_of_powerset()
#     ps_vols = {k:v[0] for k, v in ps_vols_times.items()}
#     vol = r.volume_of_powerset_to_union(ps_vols)
#
#     with open(args.output_file, 'w') as f:
#         total_time = 0
#         for k, v in ps_vols_times.items():
#             f.write("{} ; {:.2f} ; {:.2f}\n".format(k, v[0],v[1] ))
#             total_time += v[1]
#
#         f.write("{:.2f}\n".format(total_time))
# 
# else:
sum, log_vols, log_errors, error_bound = r.calculate_union_volume_iterative(args.tolerance)

with open(args.output_file, 'w') as f:
    total_time = 0
    f.write("log_vols\n")
    for e in log_vols:
        f.write("{} ; {:.2f} ; {:.2f}\n".format(e[0],e[1], e[2]))
        total_time += e[2]
    f.write("log_errors\n")
    f.write("{}\n".format(log_errors))
    f.write("error_bound\n")
    f.write("{:.2f}\n".format(error_bound))
    f.write("total_time\n")
    f.write("{:.2f}\n".format(total_time))
