Each file is a region, that is a set of polytopes.
A region file looks like this:
K
N1 M1
.
.
.
N2 M2
.
.
.
NK MK
.
.
.

Where K is the number of polytopes, for each polytope N is the number facets M is the dimension (number of real variables) followed by N linear inequations of the form b a1 … am that is interpreted as b >= aX.

On ‘/data/Dd/Nn/C’ you can find a region composed of N polytopes of dimension D. C is the case number. E. g. ‘/data/5d/7n/4’ is the case 4 of a region of 7 polytopes of dimension 5.

For a case study of case number C and dimension D we used ‘/data/Dd/ |Pr| n/C’ as provision contract and ‘/data/Dd/ |Rq| n/C’ as requirements contract. If |Pr| == |Rq| then we use instead  ‘/data/Dd/ |Pr| n/C+1’ as provision contract.