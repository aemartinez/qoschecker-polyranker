
import os, sys
cwd = os.getcwd()
sys.path.append(cwd)

import polytopesgenerator as polygen
import polytope as poly
import argparse

parser = argparse.ArgumentParser(description="Generates a batch of random regions.")
parser.add_argument('dim_start', help='dimension interval (start)', type=int)
parser.add_argument('dim_stop', help='dimension interval (stop)', type=int)
parser.add_argument('dim_step', help='dimension interval (step)', type=int)
parser.add_argument('n_start', help='number of polytopes (start)', type=int)
parser.add_argument('n_stop', help='number of polytopes (stop)', type=int)
parser.add_argument('n_step', help='number of polytopes (step)', type=int)
parser.add_argument('output_dir', help='path to output dir, region will be saved as outputdir/$dim$d/$n$n/casenumber')
parser.add_argument('-ncases', help='Number of cases for each size', default = 1, type=int)
parser.add_argument('-ncase_start', help='Starting number for numbering cases (def=1)', default = 1, type=int)
args = parser.parse_args()

for d in range(args.dim_start, args.dim_stop+1, args.dim_step):
    for n in range(args.n_start, args.n_stop+1, args.n_step):
        dir = args.output_dir + str(d)+"d/" + str(n) + "n/"
        if not os.path.exists(dir):
            os.makedirs(dir)
        for i in range(args.ncase_start, args.ncase_start+args.ncases):
            r = polygen.PolyGenerator.generate_random_region(d,n)
            r.to_file(dir + str(i))
