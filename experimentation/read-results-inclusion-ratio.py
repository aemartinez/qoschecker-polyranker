import os
import sys
import argparse
cwd = os.getcwd()
sys.path.append(cwd)

def read_results_batch(d, n_start, n_stop, n_step, m_start, m_stop, m_step, dir, case_start, case_stop, lnum, sep):

    for case in range(case_start, case_stop + 1):
        sys.stdout.write('Case ' + str(case) + ':\n')
        for n in range(n_start, n_stop + 1, n_step):
            for m in range(m_start, m_stop + 1, m_step):

                f_res = dir + str(d) + 'd/' + str(n) + 'n/' + str(m) + 'm/' + str(case)

                try:
                    f = open(f_res, 'r')
                except IOError:
                    data = '-'
                else:
                    content = f.readlines()
                    if content[0] == '-1':
                        data = 'TO'
                    else:
                        line = content[lnum]
                        data = float(line.split(",")[1])
                        data = '{:.2f}'.format(data)
                if m != m_start:
                    sys.stdout.write(' ' + sep + ' ')
                sys.stdout.write(data)

            sys.stdout.write("\n")
        sys.stdout.write("\n")

parser = argparse.ArgumentParser(description="Reads and presents results of inclusion ratio experiment.")
parser.add_argument('dim', help='dim', type=int)
parser.add_argument('n_start', help='n start', type=int)
parser.add_argument('n_stop', help='n stop', type=int)
parser.add_argument('n_step', help='n step', type=int)
parser.add_argument('m_start', help='m start', type=int)
parser.add_argument('m_stop', help='m stop', type=int)
parser.add_argument('m_step', help='m step', type=int)
parser.add_argument('-dir', help='Directory of results ("experimentation/results-inclusion-ratio/")', default="experimentation/results-inclusion-ratio/")
parser.add_argument('-case_start', help='Case number (default=1)', default=1, type=int)
parser.add_argument('-case_stop', help='Case number (default=1)', default=1, type=int)
parser.add_argument('-lnum', help='Line number to read (-1) (-1 is last)', default=-1, type=int)
parser.add_argument('-sep', help='Separator to print ("")', default='')
args = parser.parse_args()

read_results_batch(args.dim, args.n_start, args.n_stop, args.n_step, args.m_start, args.m_stop, args.m_step, args.dir, args.case_start, args.case_stop, args.lnum, args.sep)
