import os
import sys
import argparse
cwd = os.getcwd()
sys.path.append(cwd)

def reprocess_data(new_tolerance, input_file, output_file):

    with open(input_file) as file:
        lines = file.readlines()
        log_coverage_volume = eval(lines[1])
        log_coverage_errors = eval(lines[3])
        log_prov_volume = eval(lines[5])
        log_prov_errors = eval(lines[7])

    iter_coverage = found_iter(log_coverage_errors, new_tolerance)
    new_coverage_volume, new_coverage_timelapse = calculate_new_volume(log_coverage_volume, iter_coverage)

    iter_prov = found_iter(log_prov_errors, new_tolerance)
    new_prov_volume, new_prov_timelapse = calculate_new_volume(log_prov_volume, iter_prov)

    new_rank = new_coverage_volume/new_prov_volume

    with open(output_file, 'w') as file:
        file.write("coverage_volume, {:.3f}\n".format(new_coverage_volume))
        file.write("prov_volume, {:.3f}\n".format(new_prov_volume))
        file.write("coverage_steps,{}\n".format(iter_coverage))
        file.write("prov_steps,{}\n".format(iter_prov))
        file.write("rank, {:.6f}\n".format(new_rank))
        file.write("timelapse, {:.2f}\n".format(new_coverage_timelapse + new_prov_timelapse))

def found_iter(log_errors, new_tolerance):

    iter = 1
    for i in range(len(log_errors)):
        iter, bound = log_errors[i]
        if bound < new_tolerance and bound > 0:
            break

    return iter

def calculate_new_volume(log_volume, new_iter):

    new_volume = 0
    new_timelapse = 0
    for t in log_volume:
        iter, vol, timelapse = t
        if iter > new_iter:
            break
        if iter % 2 == 0:
            new_volume -= vol
        else:
            new_volume += vol
        new_timelapse += timelapse

    return new_volume, new_timelapse


parser = argparse.ArgumentParser(description="Reprocess verbose results of inclusion ratio experiment to produce data on different tolerance.")
parser.add_argument('new_tolerance', help='new_tolerance', type=float)
parser.add_argument('input_file', help='Input file')
parser.add_argument('output_file', help='Output file')
args = parser.parse_args()

reprocess_data(args.new_tolerance, args.input_file, args.output_file)
