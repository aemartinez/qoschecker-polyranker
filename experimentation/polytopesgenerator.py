'''Generates data for experimentation'''

import os, sys
cwd = os.getcwd()
sys.path.append(cwd)

import polytope as poly
import random

class PolyGenerator:
    '''A PolyGenerator'''

    @classmethod
    def generate_pseudocube(cls, dim, center_distance = 2, max_slope = 0.01):
        '''Generates a Polytope that represents a "cube" of dimension dim, with center in 0, distance to center center_distance and whose facets are tilted at most max_slope'''
        A = []
        b = []
        for i in range(dim):
            facet = [-random.uniform(0,max_slope) for x in range(i)] + [1] + [-random.uniform(0,max_slope) for x in range(i+1, dim)]
            facet_2 = [random.uniform(0,max_slope) for x in range(i)] + [-1] + [random.uniform(0,max_slope) for x in range(i+1, dim)]
            A.append(facet)
            A.append(facet_2)
            b.append(center_distance)
            b.append(center_distance)

        return poly.Polytope(A,b)

    @classmethod
    def generate_random_region(cls, dim, n):
        '''Generates a random Region with n pseudocubes of dimension dim
        Each pseudocube is translated through a random direction with the guarantee that 0 is inside the cube.
        '''

        ps = []
        for i in range(n):
            center_distance = random.uniform(2,5)
            p = cls.generate_pseudocube(dim, center_distance = center_distance)
            p.translate([random.uniform(-(center_distance-1),center_distance-1) for i in range(dim)])
            ps.append(p)

        return poly.Region(ps)
