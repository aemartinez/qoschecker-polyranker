
import os, sys
cwd = os.getcwd()
sys.path.append(cwd)

import polytopesgenerator as polygen
import polytope as poly

p = polygen.PolyGenerator.generate_pseudocube(3,radius=2)
p.to_file("experimentation/testdim3.out")
offset = [1,1,1]
p.translate(offset)
print(VolumeEstimator.estimate_volume(p))
p.to_file("experimentation/testdim3_trans.out")

exit()

p1 = poly.Polytope.from_file("experimentation/testdim3.out")
p2 = poly.Polytope.from_file("experimentation/testdim3_trans.out")

print(p1.volume)
print(p2.volume)
