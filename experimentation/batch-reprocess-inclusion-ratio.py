
import os, sys
cwd = os.getcwd()
sys.path.append(cwd)

import polytopesgenerator as polygen
import polytope as poly
import qosranker
import argparse
from iotools import IOTools
import subprocess

parser = argparse.ArgumentParser(description="Run a batch of reprocessing inclusion ratio results with new tolerance.")
parser.add_argument('dim', help='dimension', type=int)
parser.add_argument('n_start', help='number of polytopes of provision (start)', type=int)
parser.add_argument('n_stop', help='number of polytopes of provision (stop)', type=int)
parser.add_argument('n_step', help='number of polytopes of provision (step)', type=int)
parser.add_argument('m_start', help='number of polytopes of requirement (start)', type=int)
parser.add_argument('m_stop', help='number of polytopes of requirement (stop)', type=int)
parser.add_argument('m_step', help='number of polytopes of requirement (step)', type=int)
parser.add_argument('input_dir', help='path to input dir, previous results will be read from input_dir/$dim$d/$n$n/casenumber')
parser.add_argument('output_dir', help='path to output dir, region will be saved as outputdir/$dim$d/$n$n/$m$m/casenumber')
parser.add_argument('-ncase_start', help='Case numbers', default = 1, type=int)
parser.add_argument('-ncase_stop', help='Case numbers', default = 1, type=int)
parser.add_argument('-ncase_step', help='Case numbers', default = 1, type=int)
parser.add_argument('-tolerance', help='error tolerance (0.0 - 1.0)', default=0, type = float)
args = parser.parse_args()


for n in range(args.n_start, args.n_stop+1, args.n_step):
    for m in range(args.m_start, args.m_stop+1, args.m_step):

        full_results_dir = args.input_dir + str(args.dim)+"d/" + str(n)+"n/" + str(m)+"m/"
        full_output_dir = args.output_dir + str(args.dim)+"d/" + str(n) + "n/" + str(m) + "m/"
        if not os.path.exists(full_output_dir):
            os.makedirs(full_output_dir)

        for i in range(args.ncase_start, args.ncase_stop+1, args.ncase_step):

            IOTools.output("Dim: {}, N: {}, M: {}, Case: {}, Tol: {}".format(args.dim,n,m,i, args.tolerance))
            command = ['python3', 'experimentation/reprocess-inclusion-ratio-result.py', str(args.tolerance), full_results_dir + str(i), full_output_dir + str(i)]
            print(command)
            subprocess.run(command)
            IOTools.output("Fin.")
