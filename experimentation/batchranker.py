
import os, sys
cwd = os.getcwd()
sys.path.append(cwd)

import polytopesgenerator as polygen
import polytope as poly
import qosranker
import argparse
from iotools import IOTools
import subprocess

parser = argparse.ArgumentParser(description="Run a batch of ranking qos and stores the result.")
parser.add_argument('dim_start', help='dimension interval (start)', type=int)
parser.add_argument('dim_stop', help='dimension interval (stop)', type=int)
parser.add_argument('dim_step', help='dimension interval (step)', type=int)
parser.add_argument('n_start', help='number of polytopes of provision (start)', type=int)
parser.add_argument('n_stop', help='number of polytopes of provision (stop)', type=int)
parser.add_argument('n_step', help='number of polytopes of provision (step)', type=int)
parser.add_argument('-m_start', help='number of polytopes of requirement (start)', type=int, default = 0)
parser.add_argument('-m_stop', help='number of polytopes of requirement (stop)', type=int, default = 0)
parser.add_argument('-m_step', help='number of polytopes of requirement (step)', type=int, default = 0)
parser.add_argument('input_dir', help='path to input dir, regions will be read from input_dir/$dim$d/$n$n/casenumber')
parser.add_argument('output_dir', help='path to output dir, region will be saved as outputdir/$dim$d/$n$n/$m$m/casenumber')
parser.add_argument('-ncase_start', help='Case numbers for each size', default = 1, type=int)
parser.add_argument('-ncase_stop', help='Case numbers for each size', default = 1, type=int)
parser.add_argument('-ncase_step', help='Case numbers for each size', default = 1, type=int)
parser.add_argument('-timeout', help='Timeout for each ranking process', default = 3600, type=int)
parser.add_argument('-tolerance', help='error tolerance (0.0 - 1.0)', default=0, type = float)
parser.add_argument('-previous_files_dir', help='dir with files with precalculated previous iterations', default='')
args = parser.parse_args()

for d in range(args.dim_start, args.dim_stop+1, args.dim_step):
    for n in range(args.n_start, args.n_stop+1, args.n_step):
        timeouted = False
        if args.m_start == 0:
            m_start = n
            m_stop = n
            m_step = 1
        else:
            m_start = args.m_start
            m_stop = args.m_stop
            m_step = args.m_step
        for m in range(m_start, m_stop+1, m_step):
            if timeouted == True:
                break
            dir = args.output_dir + str(d)+"d/" + str(n) + "n/" + str(m) + "m/"
            if not os.path.exists(dir):
                os.makedirs(dir)
            for i in range(args.ncase_start, args.ncase_stop+1, args.ncase_step):
                input_dir = args.input_dir + str(d)+"d/"
                filename_prov = i
                filename_req = i
                if n == m:
                    filename_prov += 1

                IOTools.output("Dim: {}, N: {}, M: {}, Tol: {}, Case: {}".format(d,n,m,args.tolerance,i))

                if args.previous_files_dir == '':
                    command = ['python3', 'experimentation/rankerproc.py', input_dir + str(n) + "n/" + str(filename_prov), input_dir + str(m) + "n/" + str(filename_req), dir + str(i), '-tolerance', str(args.tolerance)]
                else:
                    previous_file = args.previous_files_dir + str(d) +"d/" + str(n) + "n/" + str(m) + "m/" + str(i)
                    command = ['python3', 'experimentation/rankerproc.py', input_dir + str(n) + "n/" + str(filename_prov), input_dir + str(m) + "n/" + str(filename_req), dir + str(i), '-tolerance', str(args.tolerance), '-previous_file', previous_file]

                try:
                    subprocess.run(command, timeout=args.timeout)
                except subprocess.TimeoutExpired:
                    with open(dir + str(i), 'w') as f:
                        f.write("-1")
                        IOTools.output("Timeout.")
                    # timeouted = True
                    # break
                else:
                    IOTools.output("Fin.")
