import os
import sys
import argparse
cwd = os.getcwd()
sys.path.append(cwd)

def read_results_batch(dir, ncases):

    for case in range(1, ncases + 1):
        f_res = dir + str(case)

        try:
            f = open(f_res, 'r')
        except IOError:
            data = '-'
        else:
            content = f.readlines()
            if content[0] == '-1':
                data = 'TO'
            else:
                line = content[-1]
                time = float(line.split(",")[1])

                line = content[-2]
                rank = float(line.split(",")[1])
                sys.stdout.write(str(case) + ', ' + str(rank) + ', ' + str(time) + '\n')

parser = argparse.ArgumentParser(description="Reads and presents results of ranking experiment.")
parser.add_argument('input_dir', help='path to input dir, ranks will be read from input_dir/')
parser.add_argument('ncases', help='number of cases', type=int)
args = parser.parse_args()

read_results_batch(args.input_dir, args.ncases)
