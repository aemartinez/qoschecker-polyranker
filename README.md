# qos-ranking

Given a qos-provision contract and a qos-requirement contract this tool estimates a coverage value that can be use to rank the service qos-compliance among other services.

## Format

Each contract is a list of polytopes in the PolyVest format (https://doi.org/10.1007/978-3-319-19647-3_6).

## Dependencies

You must have a working version of PolyVest in ./PolyVest/PolyVest. There are several polytope computation tools, in the future we may have a setup that does not depends on a specific polytope  tool.

## Usage
Execute this command to get information on how to execute the tool:

./ranker-tool.py -h

python3 ranker-tool.py -h


You can try your first rankings with this two commands:

python3 ranker-tool.py example-data/Pr-1 example-data/Rq

python3 ranker-tool.py example-data/Pr-2 example-data/Rq

You should see that Pr-2 is better than Pr-1 for this specific Rq.