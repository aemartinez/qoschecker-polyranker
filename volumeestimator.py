'''Polytope volume estimation module

This modules provides an interface to an off the shelf polytope volume estimation tool.
'''

import os
import sys
import polytope
import subprocess
from iotools import IOTools
import decorators

class VolumeEstimator:
    '''Interface to a polytope volume estimation tool.'''

    def __init__(self, executable_path = "./PolyVest/PolyVest", step_size_coef = 1600):
        '''Initializes a VolumeEstimator'''

        self.executable = executable_path
        self.temp_dir = "temp/"
        self.temp_input_file = "p"
        self.temp_output_file = "out"

        #Python3
        os.makedirs(self.temp_dir, exist_ok = True)

        #Python2.7
        #if not os.path.exists(self.temp_dir):
        #    os.makedirs(self.temp_dir)

        #PolyVest parameters
        self.step_size_coef = step_size_coef

    @decorators.counted
    def estimate_volume(self, p):
        '''Estimates volume of polytope p.'''

        # return 1 #MOCK

        if os.path.exists(self.temp_dir + self.temp_output_file):
          os.remove(self.temp_dir + self.temp_output_file)

        p.to_file(self.temp_dir + self.temp_input_file)
        command = [self.executable, self.temp_dir + self.temp_input_file, str(self.step_size_coef), self.temp_dir + self.temp_output_file]

        try:
            subprocess.run(command, check=True, stdout=subprocess.DEVNULL, stderr=subprocess.PIPE)
        except subprocess.CalledProcessError as e:
            print("Error trying to run PolyVest.", file=sys.stderr)
            print("Error message: ", e.stderr, file=sys.stderr)
            exit()

        vol = 0
        try:
            f = open(self.temp_dir + self.temp_output_file, 'r')
        except IOError as e:
            print("Error trying to run PolyVest.")
            print("Error message: ", e, file=sys.stderr)
            exit()

        else:
            content = f.readlines()
            vol = float(content[0])

        return vol
