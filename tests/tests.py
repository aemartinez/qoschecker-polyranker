'''Testing module.

This module performs unit tests over the following module'''

import os, sys
cwd = os.getcwd()
sys.path.append(cwd)

import unittest
import polytope

class UnitTestPolytope(unittest.TestCase):

    def test_region_volume_estimators(self):

        r = polytope.Region.from_file("experimentation/data/10d/3n/1")
        ps_vol = r.volume_of_powerset()
        vol1 = r.volume_of_powerset_to_union(ps_vol)
        vol2 = r.union_volume

        self.assertEqual( vol1, vol2 ) #Will fail because volume is approximated

if __name__ == '__main__':
    unittest.main()
