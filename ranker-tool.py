#!/usr/bin/env python3

import qosranker
import polytope
from iotools import IOTools
import argparse

parser = argparse.ArgumentParser(description="Given two qos-contracts calculates the inclusion ratio.")
parser.add_argument('provision', metavar='P', help='path to provision contract')
parser.add_argument('requirements', metavar='R', help='path to requirements contract')
parser.add_argument('-tolerance', metavar='Tol', default = 0, help='Tolerance for relative error', type = float)
args = parser.parse_args()

ranker = qosranker.QoSRanker()
ranking = ranker.compute_ranking(args.provision, args.requirements, args.tolerance)
print("Ranking: {}".format(ranking['ranking']))
