'''Polytope module'''

from volumeestimator import VolumeEstimator
from misc import *
import itertools
import decorators

volume_estimator = VolumeEstimator()

class Polytope:
    '''Polytope'''

    def __init__(self, A, b):
        '''Initializes a Polytope'''

        self.A = A
        self.b = b
        self._volume = None

    @classmethod
    def from_polytope(cls, p):
        '''Creates a polytope by copy'''

        res = Polytope(p.A, p.b)
        return res


    @property
    def volume(self):
        '''Estimated volume'''

        if self._volume is None:
            self._volume = volume_estimator.estimate_volume(self)

        return self._volume

    @decorators.timing
    def compute_volume(self):
        '''Compute volume'''

        return volume_estimator.estimate_volume(self)



    def translate(self, offset_vector):
        '''Translates each facet of the polytope by a certain offset. offset_vector is of the same dimension of polytope'''

        b_new = []
        for i in range(len(self.A)):
            facet_A = self.A[i]
            facet_b = self.b[i]
            offset_value = sum([facet_A[j] * offset_vector[j] for j in range(len(offset_vector))])
            final_b = self.b[i] + offset_value
            b_new.append(final_b)

        self.b = b_new

    def intersect_with(self, p2):
        '''Returns a polytope represented as the agregate of halfspaces

        PRE: dim(self) == dim(p2)
        '''

        A = self.A + p2.A
        b = self.b + p2.b
        res = Polytope(A, b)
        return res


    def to_file(self, filename, append = False):
        '''Writes down polytope to file. Format is the one accepted by PolyVest

        PRE: Polytope is not empty.
        '''

        mode = 'w'
        if append:
            mode = 'a'
        with open(filename, mode) as f:
            f.write('{:d} {:d}\n'.format(len(self.A), len(self.A[0])))
            for i in range(len(self.A)):
                f.write("{0:.2f} ".format(self.b[i]))
                for n in self.A[i]:
                    f.write("{0:.2f} ".format(n))
                f.write("\n")

    @classmethod
    def from_file(cls, filename):
        '''Reads file, creates polytopes object and returns it'''

        A = []
        b = []
        with open(filename, 'r') as f:

            content = f.readlines()
            content = content[1:]
            for fila in content:
                datos_fila = fila.split()

                b.append(float(datos_fila[0]))
                A.append([float(x) for x in datos_fila[1:]])

        p = Polytope(A, b)
        return p

    @classmethod
    def intersect_many(cls, polys):
        '''Given a list of polytopes returns the polytope intersection of all polytopes

        PRE: dimensions are equal
        '''
        A = []
        b = []

        for p in polys:
            A += p.A
            b += p.b

        res = Polytope(A, b)
        return res

class Region:
    '''A region is a list (union) of Polytopes'''

    def __init__(self, polytopes):
        '''Initializes a Region'''

        self.polytopes = polytopes
        self._sum_of_volumes = None
        self._union_volume = None

    def to_file(self, filename):
        '''Writes down polytope to file.
        File format: first line indicates number of polytopes, each polytope complies the PolyVest format.'''

        with open(filename, 'w') as f:
            f.write("%s\n" % len(self.polytopes))
        for p in self.polytopes:
            p.to_file(filename, append=True)

    def intersect_with(self, another_region):
        '''Intersects a union of Polytopes (Region) with another Region and returns a Region'''

        #Intersection of union of Pi and union of Pj is the union of pairwise intersection Pi Pj. (U_i Pi) N (U_j Pj) = U_i U_j (Pi N Pj)
        intersection_polys = []
        for pi in self.polytopes:
            for pj in another_region.polytopes:
                intersection_polys.append(pi.intersect_with(pj))

        return Region(intersection_polys)


    @property
    def sum_of_volumes(self):
        '''Sum of volumes of polytopes'''

        if self._sum_of_volumes is None:
            sum = 0
            for p in self.polytopes:
                sum += p.volume
            self._sum_of_volumes = sum

        return self._sum_of_volumes

    @property
    def union_volume(self):
        '''Volume of the union of polytopes'''

        if self._union_volume is None:
            self._union_volume, _, _ = self.calculate_union_volume_iterative()
            #self._union_volume = self._calculate_union_volume_recursive()

        return self._union_volume

    def _calculate_union_volume_recursive(self):
        '''Given a list of polytopes, calculates the volume of the union of those polytopes'''

        polys = self.polytopes

        if len(polys) == 1:
            return polys[0].volume
        elif len(polys) == 2:
            inn = polys[0].intersect_with(polys[1])
            return polys[0].volume + polys[1].volume - inn.volume
        else:
            first_poly = polys[0]
            other_polys = polys[1:]
            first_volume = self._calculate_union_volume_recursive([first_poly])
            other_volume = self._calculate_union_volume_recursive(other_polys)
            surplus_polys = Region([first_poly]).intersect_with(Region(other_polys)).polytopes
            surplus_volume = self._calculate_union_volume_recursive(surplus_polys)

            return first_volume + other_volume - surplus_volume

    def calculate_union_volume_iterative(self, error_tolerance = 0, previous_log_volume = [], previous_log_errors = []):
        '''Given a list of polytopes, calculates the volume of the union of those polytopes.
        If previous_log_volume and previous_log_errors are not empty it is taken as precalculated information. 
        It may be the case that this information is enough to give an answer depending on the error_tolerance needed.
        '''

        polys = self.polytopes

        error_bound = -1
        log_vols = previous_log_volume
        log_errors = previous_log_errors

        sum, time = calculate_new_volume(log_vols, len(log_errors)+1)

        start_i = 1
        if len(log_errors) > 0:
            start_i = len(log_errors) + 2

        if start_i > 1:
            error_bound = log_errors[-1][1]
            if error_tolerance != 0 and error_bound < error_tolerance and error_bound > 0:
                return sum, log_vols, log_errors, error_bound

        for i in range(start_i, len(polys) + 1):
            iteration_distance = 0
            for p in Region._intersections_taken_k(polys, i):
                vol = p.compute_volume()
                log_vols.append((i, vol, p.compute_volume.timelapse))
                iteration_distance += vol

            prev_sum = sum
            if i % 2 == 0:
                sum -= iteration_distance
            else:
                sum += iteration_distance

            if i > 1:
                error_bound = iteration_distance / min(sum, prev_sum)
                log_errors.append((i, error_bound))
                # print(error_bound)
                if error_tolerance != 0 and error_bound < error_tolerance and error_bound > 0:
                    return sum, log_vols, log_errors, error_bound

        return sum, log_vols, log_errors, 0

    @classmethod
    def _intersections_taken_k(cls, polys, k):
        '''Iterates through all possible intersections of taking k polytopes from polys'''

        for ps in itertools.combinations(polys, k):
            p = Polytope.intersect_many(ps)
            yield p

    @decorators.timing
    def volume_of_powerset_to_union(self, powerset):
        '''Given the volume of all the possible intersections of region (powerset) it computes the volume of union using inclusion exclusion principle
            powerset is dict : string -> float. E.g. {'(1,)': 1, '(2,)': 1, '(3,)': 1, '(1, 2)': 1, '(1, 3)': 1, '(2, 3)': 1, '(1, 2, 3)': 1}
        '''

        sum = 0
        indexes = range(1, len(self.polytopes) + 1)
        for k in range(1, len(self.polytopes) + 1):
            for p in itertools.combinations(indexes, k):
                if k % 2 == 0:
                    sum -= powerset[str(p)]
                else:
                    sum += powerset[str(p)]

        return sum

    @decorators.timing
    def volume_of_powerset(self):
        '''For each subset of region it computes the volume of the intersection and the timelapse'''

        res = dict()

        index_powerset = []
        for tup in powerset(range(1, len(self.polytopes)+1)):
            index_powerset.append(str(tup))

        polys_powerset = list(powerset(self.polytopes))

        for i in range(len(polys_powerset)):
            p = Polytope.intersect_many(polys_powerset[i])
            res[index_powerset[i]] = (p.compute_volume(), p.compute_volume.timelapse)

        return res

    @classmethod
    def from_file(cls, filename):
        '''Reads file, creates Region object and returns it'''

        with open(filename, 'r') as f:

            polytopes = []
            content = f.readlines()
            index = 0
            n_polys = int(content[index])
            index += 1
            for i in range(0, n_polys):
                A = []
                b = []
                m, n = map(int, content[index].split())
                index += 1
                for j in range(0, m):
                    data = content[index].split()

                    b.append(float(data[0]))
                    A.append([float(x) for x in data[1:]])
                    index += 1

                polytopes.append(Polytope(A, b))

            return Region(polytopes)


def calculate_new_volume(log_volume, new_iter):

    new_volume = 0
    new_timelapse = 0
    for t in log_volume:
        iter, vol, timelapse = t
        if iter > new_iter:
            break
        if iter % 2 == 0:
            new_volume -= vol
        else:
            new_volume += vol
        new_timelapse += timelapse

    return new_volume, new_timelapse
