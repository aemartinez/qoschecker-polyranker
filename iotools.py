'''Input/Output Tools'''

from datetime import datetime
import sys

class IOTools:
    '''IOTools'''

    def __init__(self):
        '''Initializes IOTools'''

        return

    @classmethod
    def output(cls, msg):
        sys.stderr.write("%s %s\n" % (datetime.now(), msg))
