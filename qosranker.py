'''QoS Ranker module'''

import polytope as poly
import decorators

class QoSRanker:
    '''Performs the ranking method'''

    def __init__(self):
        '''Initializes a Ranker, it might have different parameters for the volume estimation'''

        return

    @decorators.timing
    def compute_ranking(self, prov_file, req_file, tolerance = 0, previous_log_coverage_volume = [], previous_log_coverage_errors = [], previous_log_prov_volume = [], previous_prov_error_bound = []):
        '''Calculates 'coverage' of provision contract (prov_file) over requirement contract (req_file). Each contract file is a list of polytopes represented as systems of linear inequalities
        If previous_log_volume and previous_log_errors are not empty it is taken as precalculated information.
        It may be the case that this information is enough to give an answer depending on the error_tolerance needed.
        '''

        prov_region = poly.Region.from_file(prov_file)
        req_region = poly.Region.from_file(req_file)

        list_of_coverages = []
        for p in prov_region.polytopes:
            polys = []
            for r in req_region.polytopes:
                inn = p.intersect_with(r)
                list_of_coverages.append(inn)

        coverage_region = poly.Region(list_of_coverages)

        #return {'ranking': 1, 'coverage_volume':2, 'prov_volume': 2, 'estimator_calls': 3} #MOCK

        coverage_volume, log_coverage_volume, log_coverage_errors, coverage_error_bound = coverage_region.calculate_union_volume_iterative(tolerance, previous_log_coverage_volume, previous_log_coverage_errors)
        prov_volume, log_prov_volume, log_prov_errors, prov_error_bound = prov_region.calculate_union_volume_iterative(tolerance, previous_log_prov_volume, previous_prov_error_bound)

        return {'ranking': coverage_volume/prov_volume, 'coverage_volume':coverage_volume, 'coverage_error_bound': coverage_error_bound, 'log_coverage_volume': log_coverage_volume, 'log_coverage_errors':log_coverage_errors, 'prov_volume': prov_volume, 'prov_error_bound': prov_error_bound, 'log_prov_volume':log_prov_volume, 'log_prov_errors':log_prov_errors, 'estimator_calls': poly.volume_estimator.estimate_volume.calls}
