import polytope
import argparse

parser = argparse.ArgumentParser(description="Estimates volume of region.")
parser.add_argument('region', metavar='R', help='path to region file')
args = parser.parse_args()


r = polytope.Region.from_file(args.region)
print("Number of polys: {}".format(len(r.polytopes)))
print("Volume: {}".format(r.union_volume))
